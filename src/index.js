'use strict';

const createSessionExCreator = require('express-session');
const SessionStore           = require('connect-redis')(createSessionExCreator);
const parseCookie            = require('cookie-parser')();

/**
 * Create a session creator.
 * @param opts
 */
function createSessionCreator(opts) {
	const createSessionEx = createSessionExCreator(Object.assign({}, opts, {
		store: new SessionStore(opts.store)
	}));
	const createSessionIo = (sck, next) => {
		const req = sck.client.request;
		const res = {end() {}};
		parseCookie(req, res, (err) => {
			if (err) {
				return next(err);
			}
			createSessionEx(req, res, next);
		});
	};
	return {
		ex: createSessionEx,
		io: createSessionIo
	};
}

module.exports = createSessionCreator;
